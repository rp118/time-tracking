
import React, { Component } from 'react';
import './App.css';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';



class EditTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
        columnDefs: [
            {headerName: "Billing Category", field: "cat"},
            {headerName: "Activity", field: "activity"},
            {headerName: "Sun", field: "sun", editable: true},
            {headerName: "Mon", field: "mon"},
            {headerName: "Tue", field: "tue"},
            {headerName: "Wed", field: "wed"},
            {headerName: "Thu", field: "thu"},
            {headerName: "Fri", field: "fri"},
            {headerName: "Sat", field: "sat"},
            {headerName: "Total", field: "total"},
            {headerName: "Comments", field: "comments"}

        ],
        rowData: [
            {cat: "Week 44 (12.16.18 – 12.22.18)", activity: 'Construction', mon: 8,tue: 8, wed: 8, thu: 8, fri: 8, sat: 8, total: 40, comments: 'Worked on FSIA'},
            
        ]
    }
}

  render() {
    return (
        <div style={{
            height: "100%",
            width: "100%"
          }}
            className="ag-theme-material"
        >
            <AgGridReact
                columnDefs={this.state.columnDefs}
                rowData={this.state.rowData}>
            </AgGridReact>
        </div>
        
    );
}
}

export default EditTable;
