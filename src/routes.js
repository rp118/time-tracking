import React from 'react'
import { Route, HashRouter, Switch } from 'react-router-dom'
import Dashboard from './Dashboard/Dashboard'
import SubmitTimesheet from './Timesheet/SubmitTimesheet'
import ApproveTimesheet from './Timesheet/ApproveTimesheet'
import AdminScreen from './Admin/AdminScreen';


export default props => (
    <HashRouter>
      <Switch>
          <Route exact path='/' component={ Dashboard } />
          <Route exact path='/SubmitTimesheet' component={ SubmitTimesheet } />
          <Route exact path='/ApproveTimesheet' onclick='window.history.pushState(null,null)' component={ ApproveTimesheet } />
          <Route exact path='/Admin' component={ AdminScreen } />
      </Switch>
    </HashRouter>
  )