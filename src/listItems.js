import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import TimeSheetIcon from '@material-ui/icons/Schedule';
import ApproverIcon from '@material-ui/icons/DoneAll';
import TimeOffIcon from '@material-ui/icons/Event';
import ReportIcon from '@material-ui/icons/Timeline';
import AdminIcon from '@material-ui/icons/VerifiedUser';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { Link, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  listItemFormat: {
    paddingTop: '5px',
    paddingBottom: '5px',
  },
});
export const mainListItems = (
  <div>
    <ListItem component={Link} to={{pathname: '/'}} button  >
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItem>
    <ListItem component={Link} to={{pathname: '/SubmitTimesheet'}} button >
      <ListItemIcon>
        <TimeSheetIcon />
      </ListItemIcon>
      <ListItemText primary="Submit Timesheet" />
    </ListItem>
    <ListItem button component={Link} to={{pathname: '/ApproveTimesheet'}}>
      <ListItemIcon>
        <ApproverIcon />
      </ListItemIcon>
      <ListItemText primary="Approve Timesheet" />
    </ListItem>
    <ListItem button >
      <ListItemIcon>
        <TimeOffIcon />
      </ListItemIcon>
      <ListItemText primary="Submit Timeoff" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <ReportIcon />
      </ListItemIcon>
      <ListItemText primary="Reports" />
    </ListItem>
    <ListItem component={Link} to={{pathname: '/Admin'}} button >
      <ListItemIcon>
        <AdminIcon />
      </ListItemIcon>
      <ListItemText primary="Admin Panel" />
    </ListItem>
    <ListItem button >
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="User Management" />
    </ListItem>
  </div>
);
