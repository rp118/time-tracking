import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  
}))(TableCell);

const CustomTableCellTitle = withStyles(theme => ({
  head: {
    backgroundColor: '#1a237e',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  cellFormat:{
    padding: '4px 4px 4px 4px',
    align: 'center',
  },
  rowFormat:{
    height: '25px',
  },
  paginationRowFormat:{
    height: '35px',
  }
};

let id = 0;
function createData(name, calories, fat, carbs, proteins) {
  id += 1;
  return { id, name, calories, fat, carbs, proteins};
}

const data = [
  createData('Week 44 (12.16.18 – 12.22.18)', 0,0, 0, 'Not Submitted'),
  createData('Week 44 (12.16.18 – 12.22.18)', 0,0, 0, 'Overdue'),
  createData('Week 44 (12.16.18 – 12.22.18)', 0,0, 0, 'Approved'),
  createData('Week 44 (12.16.18 – 12.22.18)', 0,0, 0, 'Rejected')

];

function TimesheetTable(props) {
  const { classes } = props;

  return (
    <Paper className={classes.root}>
    <div>
      <Table className={classes.table}>
        <TableHead>

          <TableRow className={classes.rowFormat}>
            <CustomTableCell align="center">Timesheet Period</CustomTableCell>
            <CustomTableCell align="center">Billable Hours</CustomTableCell>
            <CustomTableCell align="center">Non-Billable Hours</CustomTableCell>
            <CustomTableCell align="center">Total</CustomTableCell>
            <CustomTableCell align="center">Status</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => (
            <TableRow key={n.id} className={classes.rowFormat}>
              <TableCell align="center" component="th" scope="row">
                {n.name}
              </TableCell>
              <TableCell align="center">{n.calories}</TableCell>
              <TableCell align="center">{n.fat}</TableCell>
              <TableCell align="center" >{n.carbs}</TableCell>
              <TableCell align="center" >{n.proteins}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      </div>
      <TablePagination 
        height="35px"
          rowsPerPageOptions={[2, 10, 25]}
          component="div"
          count="4"
          rowsPerPage="2"
          page='0'
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
         
        />
    </Paper>
  );
}

TimesheetTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimesheetTable);