import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#90caf9',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);


const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

let id = 0;
function createData(name, calories, fat, carbs) {
  id += 1;
  return { id, name, calories, fat, carbs};
}

const data = [
  createData('1/10/2018', '1/12/2018',2, 'Approved'),
  createData('2/15/2018', '2/28/2018',10, 'Rejected'),
  createData('3/19/2018', '3/20/2018',2, 'Pending Approval')
];

function SimpleTable(props) {
  const { classes } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomTableCell align="center">Start Date</CustomTableCell>
            <CustomTableCell align="center">End Date</CustomTableCell>
            <CustomTableCell align="center"># Working Days</CustomTableCell>
            <CustomTableCell align="center">Status</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => (
            <TableRow key={n.id}>
              <TableCell align="center" component="th" scope="row">
                {n.name}
              </TableCell>
              <TableCell align="center">{n.calories}</TableCell>
              <TableCell align="center">{n.fat}</TableCell>
              <TableCell align="center" color="green" >{n.carbs}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);