import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Grid, Button } from '@material-ui/core';
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);
const CustomTableCellApproved = withStyles(theme => ({
body: {
    fontSize: 14,
    color: 'green',
},
}))(TableCell);

  
const styles = theme =>({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  rowFormat:{
    height: '25px',
  },
  companyRed: { // doesnt work - grey button
    main: '#E44D69',
    contrastText: '#000',
  },
  reject:{
    backgroundColor: 'red'
  },
  approve:{
    color: 'green'
  },
  button: {
    marginLeft: theme.spacing.unit ,
    //marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
  },
  summary: {
    backgroundColor: '#1a237e',
  },
  text:{
    color: 'white',
  },
  nopadding:{
    padding: '0px 0px 0px 0px',
  },
});

let id = 0;
function createData(submitter, period, billHr, nonBillHr, Total,Status) {
  id += 1;
  return { id,submitter, period, billHr, nonBillHr, Total,Status};
}

const data = [
  createData('Maduraiveeran, Rajesh','Week 42 (12.02.18 – 12.08.18)','28','12','40','Approved'),
  createData('Kumar, Rajesh','Week 41 (12.02.18 – 12.08.18)','44','0','44','Approved'),
];

function ApprovedTimesheets(props) {
  const { classes } = props;
  const children = [];
  
  
  for(var i = 0; i < data.length; i += 1){
    children.push(<MainRow columnValues={data[i]}></MainRow>);
    
  };
  return (
    <ExpansionPanel className={classes.root}>
    <ExpansionPanelSummary className={classes.summary} expandIcon={<ExpandMoreIcon className={classes.text}/>}>
  <Typography className={classes.text}>Timesheets I Approved</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails className={classes.nopadding}>
  
    <Paper className={classes.root}>
      
      <Table className={classes.table}>
        <TableHead>
         <TableRow className={classes.rowFormat}>
                <CustomTableCell align="center" >Submitter</CustomTableCell>
                <CustomTableCell align="center">Timesheet Period</CustomTableCell>
                <CustomTableCell align="center">Billable hours</CustomTableCell>
                <CustomTableCell align="center">Non-Billable hours</CustomTableCell>
                <CustomTableCell align="center">Total</CustomTableCell>
                <CustomTableCell align="center">Status</CustomTableCell>
        </TableRow>
        </TableHead>
        <TableBody>
          {children}
          
        </TableBody>
      </Table>
    </Paper>
    </ExpansionPanelDetails>
      </ExpansionPanel>
  );

  function MainRow(props) {
    const n = props.columnValues;
    return (
            <TableRow key={n.id} className={classes.rowFormat}>
              <TableCell align="center" scope="row">{n.submitter}</TableCell>
              <TableCell align="center" scope="row">{n.period}</TableCell>
              <TableCell align="center" scope="row">{n.billHr}</TableCell>
              <TableCell align="center" scope="row">{n.nonBillHr}</TableCell>
              <TableCell align="center" scope="row">{n.Total}</TableCell>
              <CustomTableCellApproved align="center" scope="row">{n.Status}</CustomTableCellApproved>
            </TableRow>
    );
  }
}

ApprovedTimesheets.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ApprovedTimesheets);