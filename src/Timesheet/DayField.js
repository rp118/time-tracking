import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/Input';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({

  textField: {
   // marginLeft: theme.spacing.unit,
   // marginRight: theme.spacing.unit,
    width: 35,
    height: '25px',
    padding: '0px 0px 0px 0px'
  }
});

class DayField extends React.Component {
  state = {
    age: '',
    name: 'hai',
    labelWidth: 0,
  };


  render() {
    const { classes } = this.props;

    return (
    
            <Input
            inputStyle={{ textAlign: 'center', padding:'0px 0px 0px 0px' }}
            id="standard-bare"
            className={classes.textField}
            //defaultValue="0"
           // margin="normal"
            inputProps={{maxLength: 4}}
            />
           
     
    );
  }
}

DayField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DayField);