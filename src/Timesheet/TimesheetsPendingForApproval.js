import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Grid, Button } from '@material-ui/core';
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);
const CustomTableCellOverDue = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: 'red',
    fontWeight: 'bold',
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);



const styles = theme =>({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  rowFormat:{
    height: '25px',
  },
  companyRed: { // doesnt work - grey button
    main: '#E44D69',
    contrastText: '#000',
  },
  reject:{
    backgroundColor: 'red'
  },
  approve:{
    backgroundColor: 'green'
  },
  button: {
    marginLeft: theme.spacing.unit ,
    //marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
  },
  paddingBottom:{
    paddingBottom:'10px',
  },
});

let id = 0;
function createData(cat, act, sun, mon, tue,wed,thu,fri,sat,total,comment) {
  id += 1;
  return { id,cat, act, sun, mon, tue,wed,thu,fri,sat,total,comment};
}

const data = [
  createData('Billing category','Activity','Sun','Mon','Tue','Wed','Thu','Fri','Sat','Total'),
  createData('AVS','System Testing','0','0','2','3','4','5','6','20','Conducted research on the CMS interfaces'),
  
  createData('AVS – IMMR','System Testing','0','0','2','3','4','5','6','20','Teste comment'),
];

function TimesheetsPendingForApproval(props) {
  const { classes } = props;
  const children = [];
  
  
  for(var i = 0; i < data.length; i += 1){
    children.push(<MainRow columnValues={data[i]}></MainRow>);
    if(i!=0)
    children.push(<CommentRow columnValues={data[i]}></CommentRow>);
  };
  return (
    
    <Paper className={classes.root}>
      <div className={classes.paddingBottom}>
      <Grid container xs={12} nowrap>
      <Table className={classes.table}>
        <TableHead>
        <TableRow className={classes.rowFormat}>
                <CustomTableCell align="center" colSpan={2}>Rajesh Kumar</CustomTableCell>
                <CustomTableCell align="center" colSpan={4}>Week 44 (12.16.18 – 12.22.18)</CustomTableCell>
                <CustomTableCellOverDue align="center" colSpan={4}>Approval Overdue</CustomTableCellOverDue>
        </TableRow>
        </TableHead>
        <TableBody>
          {children}
          
        </TableBody>
      </Table>
      </Grid>
      </div>
      <Grid container
        direction="row"
        justify="flex-end"
        alignItems="flex-end">
        <Grid container
        direction="row"
        justify="flex-end"
        xs={1}>
        <Button size="small" className={classes.button} variant="contained"  color = "primary"  className={classes.approve}>
            Approve
        </Button>
        </Grid>
        <Grid container
        direction="row"
        justify="flex-end"
       xs={1}>
        <Button size="small" className={classes.button} variant="contained" color = "primary"  className={classes.reject}>
            Reject
        </Button>
      </Grid>
      </Grid>
    </Paper>
    
  );

  function MainRow(props) {
    const n = props.columnValues;
    return (
            <TableRow key={n.id} className={classes.rowFormat}>
              <TableCell align="left" scope="row">{n.cat}</TableCell>
              <TableCell align="left" scope="row">{n.act}</TableCell>
              <TableCell align="left" scope="row">{n.sun}</TableCell>
              <TableCell align="left" scope="row">{n.mon}</TableCell>
              <TableCell align="left" scope="row">{n.tue}</TableCell>
              <TableCell align="left" scope="row">{n.wed}</TableCell>
              <TableCell align="left" scope="row">{n.thu}</TableCell>
              <TableCell align="left" scope="row">{n.fri}</TableCell>
              <TableCell align="left" scope="row">{n.sat}</TableCell>
              <TableCell align="left" scope="row">{n.total}</TableCell>
            </TableRow>
    );
  }
  function CommentRow(props) {
    const n = props.columnValues;
    return (
            <TableRow key={n.id} className={classes.rowFormat}>
              <TableCell align="left" scope="row" colSpan={10}> {n.comment}</TableCell>
            </TableRow>
    );
  }
}

TimesheetsPendingForApproval.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimesheetsPendingForApproval);