import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types';
import React from 'react';
import axios from 'axios';
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  
}))(TableCell);

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  overDue:{
    color: 'red',
    fontSize: 14,
  },
  pending:{
    fontSize: 14,
    color: 'orange',
  },
  table: {
    minWidth: 700,
  },
  cellFormat:{
    padding: '4px 4px 4px 4px',
    align: 'center',
  },
  rowFormat:{
    height: '25px',
  },
  paginationRowFormat:{
    height: '35px',
  },
  floatRight:{
    float:'right',
  },
  summary: {
    backgroundColor: '#1a237e',
  },
  text:{
    color: 'white',
  },
  nopadding:{
    padding: '0px 0px 0px 0px',
  },
 
};

let id = 0;
function createData(name, calories, fat, carbs, proteins) {
  id += 1;
  return { id, name, calories, fat, carbs, proteins};
}

const data = []; 

class TimesheetApprovalTable extends React.Component { 
  
  state = {
    json:[],
    callMade:false,
  };
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log(this.state.callMade);
    if(!this.state.callMade){
      this.TimesheetApprovalTable();
    }
    console.log(this.state.callMade);
  }
  TimesheetApprovalTable(){
    this.setState({ json: [] });
    axios.get('http://localhost:8080/fw/test')
     .then(( results ) =>  this.setState({ json: results.data}))
     .then(this.setState({ callMade:true}));
  }

  render() {
  const { classes } = this.props;
  let children = [];
  
  let timesheets =  this.state.json;
  if(timesheets){
      if(timesheets.timesheet){
        const tableData = Object.values(timesheets.timesheet);
      tableData.forEach((prop, key) => {
        data.push(createData(prop.period, prop.billableHours, prop.nonBillableHours, prop.total, prop.status))
      });
      }
    }
  for(var i = 0; i < data.length; i += 1){
    if(data[i].proteins.trim()=='Approval Overdue'){
      children.push(<OverDueRow columnValues={data[i]}></OverDueRow>);
    }else{
      children.push(<PendingRow columnValues={data[i]}></PendingRow>);
    }
  };

  
  return (
      <ExpansionPanel className={classes.root} defaultExpanded padding={0} >
        <ExpansionPanelSummary className={classes.summary}  expandIcon={<ExpandMoreIcon className={classes.text} />}>
      <Typography className={classes.text}>Timesheet Pending Approval</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.nopadding}>
          <Paper className={classes.root}>
        <Table className={classes.table}>
        <TableHead>
          <TableRow className={classes.rowFormat} id="TableHead" show={this.state.showResults}>
            <CustomTableCell align="center">Timesheet Period</CustomTableCell>
            <CustomTableCell align="center">Billable Hours</CustomTableCell>
            <CustomTableCell align="center">Non-Billable Hours</CustomTableCell>
            <CustomTableCell align="center">Total</CustomTableCell>
            <CustomTableCell align="center">Status</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody id="child" show={this.state.showResults}>
         {children}
        </TableBody>
      </Table>
      </Paper>
      </ExpansionPanelDetails>
      </ExpansionPanel>
  );

  function OverDueRow(props) {
    const n = props.columnValues;
      return (
      <TableRow key={n.id} className={classes.rowFormat}>
      <TableCell align="center" component="th" scope="row">
        {n.name}
      </TableCell>
      <TableCell align="center">{n.calories}</TableCell>
      <TableCell align="center">{n.fat}</TableCell>
      <TableCell align="center" >{n.carbs}</TableCell>
      <TableCell align="center" className={classes.overDue}>{n.proteins}</TableCell>
    </TableRow>
    );
  }
  function PendingRow(props) {
    const n = props.columnValues;
    return (
      <TableRow key={n.id} className={classes.rowFormat}>
      <TableCell align="center" component="th" scope="row">
        {n.name}
      </TableCell>
      <TableCell align="center">{n.calories}</TableCell>
      <TableCell align="center">{n.fat}</TableCell>
      <TableCell align="center" >{n.carbs}</TableCell>
      <TableCell align="center" className={classes.pending}>{n.proteins}</TableCell>
      </TableRow>
    );
  }
 
}
}
TimesheetApprovalTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimesheetApprovalTable);