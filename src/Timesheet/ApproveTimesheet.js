import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import TimesheetsPendingForApproval from './TimesheetsPendingForApproval';
import TimesheetApprovalTable from './TimesheetApprovalTable';
import ApprovedTimesheets from './ApprovedTimesheets';
import Navigation from '../Navigation';
import Header from '../Header';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';
const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  tableContainer: {
    height: 250,
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
  title:{
    backgroundColor: '#1a237e',
    color: theme.palette.common.white,
   }
});

class ApproverTimeSheetView extends React.Component {
  state = {
    open: true,
    person: [],
  };
  constructor(props) {
    super(props);
  }
  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };
  
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header handleClick={this.handleDrawerOpen} open={this.state.open}/>
        <Navigation handleClick={this.handleDrawerClose} open={this.state.open}/>
        <Grid container spacing={0} justify="flex-start">
        
        <main className={classes.content}>
          <div className={classes.appBarSpacer} color="primary"/>
          <div className={classes.h5}>     
          
            <TimesheetApprovalTable />
          
          </div>
          <div className={classes.h5}>
           <Grid container spacing={10}>
              <TimesheetsPendingForApproval />
            </Grid>
          </div>
          <div className={classes.h5}>
            <Grid container spacing={10}>
              <ApprovedTimesheets />
            </Grid>
            </div>
        </main>
        </Grid>
      </div>
    );
  }
}

ApproverTimeSheetView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ApproverTimeSheetView);