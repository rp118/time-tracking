import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import EditTable from '../EditTable';
import Header from '../Header';
import Navigation from '../Navigation';
import TimeSheet from './TimeSheet';
import InputLabel from '@material-ui/core/InputLabel';
import { MuiPickersUtilsProvider} from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

import WeekPicker from "./WeekPicker";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { green } from '@material-ui/core/colors';
const drawerWidth = 240;

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: theme.palette.common.white,
    width: '50px'
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 0, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 0px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),

    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  tableContainer: {
    height: 320,
    //width: '80%',
  },
  status:{
    color:'green',
    fontSize:'12px',
    fontWeight: 'bold',
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
  button: {
    marginLeft: theme.spacing.unit ,
    //marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
  },
  label: {
    marginLeft: theme.spacing.unit * 2,
   // marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  },
  formControl: {
    // margin: theme.spacing.unit,
     minWidth: 200,
   },
   gridControl:{
    marginTop: theme.spacing.unit * 2,

   },
   title:{
    backgroundColor: '#1a237e',
    color: theme.palette.common.white,
   },
   weekPicker:{
    maxWidth: 20,
   }
});

class SubmitTimesheet extends React.Component {
  constructor(){  
    super();

    this.state = {
    name:'',
    open: true
    };
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
  }

  handleDrawerOpen = () => {
   console.log(this.state.open);
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    console.log(this.state.open);
    this.setState({ open: false });
  };

  handleChange = event => {
 //   console.log(event.target.name);
  this.setState({ [event.target.name]: event.target.value });
};
  render() {
    const { classes } = this.props;
    const { selectedDate } = this.state;
    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header handleClick={this.handleDrawerOpen} open={this.state.open}/>
        <Navigation handleClick={this.handleDrawerClose} open={this.state.open}/>
        
        <main className={classes.content}>
          <div className={classes.appBarSpacer} color="primary"/>
          <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              align="center"
            >
          
            Submit Timesheet
          </Typography>
          
          <Grid container spacing={0} justify="flex-start">
         
          <Grid item xs={3} className={classes.gridControl}>
          
          <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-simple">First Name, Last Name</InputLabel>
          <Select value={this.state.name}
            onChange={this.handleChange} className={classes.formControl}  name="name" >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Elangovan Shanmugam</MenuItem>
            <MenuItem value={20}>Rajesh Maduraiveeran</MenuItem>
            <MenuItem value={30}>Humesh Shukla</MenuItem>
          </Select>
        </FormControl>
        
              </Grid>
              
              
          <Grid item xs={4}  className={classes.gridControl} nowrap>
          <Grid container spacing={0} justify="flex-start" className={classes.gridControl} nowrap>
          
         
          <ArrowLeftIcon align="right" className={classes.button} />
          
          
          
          <MuiPickersUtilsProvider utils={DateFnsUtils} minWidth >
          
        <WeekPicker />
       
        </MuiPickersUtilsProvider>
        
        
        
        <ArrowRightIcon align="center" className={classes.button} />
        
        
        </Grid>
          </Grid>
          
          
          <Grid item xs={5} className={classes.gridControl} justify="flex-end">
          <Grid container container
  direction="row"
  justify="flex-end"
  alignItems="flex-end">
          <Grid item xs className={classes.gridControl} >
          <Button size="small" className={classes.button} variant="contained" color="primary" className={classes.button}>
           Edit
          </Button>
          <Button size="small" className={classes.button} variant="contained" color="primary" className={classes.button}>
            Save
          </Button>         
          <Button  size="small" className={classes.button} variant="contained" color="primary" className={classes.button}>
            Submit
          </Button>
          <Button size="small" className={classes.button} variant="contained" color="primary" className={classes.button}>
            Print
          </Button>
          <Button size="small" className={classes.button} variant="contained" color="primary" className={classes.button}>
            Email
          </Button>
          </Grid>
          </Grid>
          </Grid>
         
        </Grid>
        <Grid item xs={12} className={classes.gridControl}>
        <Grid container container
          direction="row"
          justify="flex-end"
          alignItems="flex-end">
                Billable Hours: <div className={classes.status}>XX Hours</div>&nbsp;
                Non-Billable Hours: <div className={classes.status}>XX Hours</div>&nbsp;
                Status: <div className={classes.status}>Approved</div>
        </Grid>
        </Grid>
          <div className={classes.tableContainer}>
            <TimeSheet />
          </div>
         
         
        </main>
      </div>
    );
  }
}

SubmitTimesheet.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SubmitTimesheet);