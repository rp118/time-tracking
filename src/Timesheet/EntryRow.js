import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DayField from './DayField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';

const styles = theme => ({
root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  formControl: {
   // margin: theme.spacing.unit,
    minWidth: 150,
    height: '25px',
  },
  button: {
    marginLeft: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  },
  table: {
   minWidth: 700,
  // maxWidth: 1000,
   padding: 'none',
  },
  cell: {
    width:'20%'
  },
  cellLarge: {
    width:'60%'
  },
  textFieldLarge:{
    width: '70%'

  },
  cellFormat:{
    padding: '4px 4px 4px 20px',
    align: 'center',
    height: '25px',
    borderBottom: 'none'
  },
  rowFormat:{
    height: '25px',
  }

});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return { id, name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

class EntryRow extends React.Component {
  state = {
    activity: '',
    category: '',
    labelWidth: 0,
  };
  handleChange = event => {
      console.log(event.target.name);
    this.setState({ [event.target.name]: event.target.value });
  };


  render() {
    const { classes } = this.props;

    return (
     
           <TableRow className={classes.rowFormat}>
              <TableCell  className={classes.cellFormat} align='left'>
              <Select value={this.state.category}
            onChange={this.handleChange} className={classes.formControl}  name="category">              
              <MenuItem value={10}>AVS</MenuItem>
              <MenuItem value={20}>AVS-IMMR</MenuItem>
              <MenuItem value={30}>FSIA</MenuItem>
              </Select>
              </TableCell>
               <TableCell  className={classes.cellFormat} align='left'>
              <Select value={this.state.activity}
            onChange={this.handleChange} className={classes.formControl}  name="activity">              
              <MenuItem value={10}>Research</MenuItem>
              <MenuItem value={20}>Construction</MenuItem>
              <MenuItem value={30}>SYS Testing</MenuItem>
              </Select>
              </TableCell >
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
              <TableCell  className={classes.cellFormat} align='left'><DayField/></TableCell>
           </TableRow>
                  
      
           
   
      
    );
  }
}

EntryRow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EntryRow);