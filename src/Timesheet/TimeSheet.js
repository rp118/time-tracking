import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DayField from './DayField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import EntryRow from './EntryRow';
import TablePagination from '@material-ui/core/TablePagination';
import { createMuiTheme } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import classNames from 'classnames';

const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      root: {
       // padding: '40px 24px 40px 16px',
        backgroundColor: 'lightblue'
      },
      paddingDefault: {
               padding: '40px 24px 40px 16px',
             },
    }
  }
});


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#1976d2',
    color: theme.palette.common.white,
    width: '50px'
  },
  body: {
    fontSize: 14,
  }
}))(TableCell);

const styles = theme => ({
root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',

  },
  formControl: {
   // margin: theme.spacing.unit,
    minWidth: 150,
  },
  button: {
    marginLeft: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  },
  table: {
   minWidth: 700,
   //maxWidth: 1000,
   padding: 'none',
  },
  cell: {
    width:'20%'
  },
  cellLarge: {
    width:'60%'
  },
  textFieldLarge:{
    width: '50%',
   // height: '30px'

  },
  cellFormat:{
    padding: '4px 4px 4px 15px',
    align: 'center',
  },
  rowFormat:{
    height: '25px',
  },
  bootstrapRoot: {
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
    width:"50%"
  },
   bootstrapInput: {
    borderRadius: 4,
    width: '50%',
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: 'auto',
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },

});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return { id, name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

class TimeSheet extends React.Component {
  state = {
    numChildren: 1
  }

  getInitialState(){
    return [
        {id:1,name:"Some Name"}
    ]
  } ;
  onAddChild = () => {
    this.setState({
      numChildren: this.state.numChildren + 1
    });
  };
  render() {
    const { classes } = this.props;
    const children = [];

    for (var i = 0; i < this.state.numChildren; i += 1) {
      children.push(<EntryRow key={i} number={i} />);
      children.push(	<TableRow className={classes.rowFormat} >
        <TableCell colSpan={10}> 
       {/*} <InputBase
          id="bootstrap-input"
          defaultValue="Comments"
          fullWidth= {true}
          classes={{
            root: classes.bootstrapRoot,
            input: classes.bootstrapInput,
          }}/> */}
          
          <TextField
          id="standard-dense"
          label="Comments"
          className={classNames(classes.textField, classes.dense,classes.textFieldLarge)}
          margin="dense"
        />
        
     
         </TableCell>           
      </TableRow>   );
    };
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
       
        <TableHead>
           <TableRow className={classes.rowFormat}>
              <CustomTableCell className={classes.cellFormat} align='left'>Billing Category</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Activity</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Sun</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Mon</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Tue</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Wed</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Thu</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Fri</CustomTableCell>
              <CustomTableCell className={classes.cellFormat} align='left'>Sat</CustomTableCell>            
              <CustomTableCell className={classes.cellFormat} align='left'>Total</CustomTableCell>
           </TableRow>
        </TableHead>
        <TableBody>
        
        {children}
         
        </TableBody>
     </Table>
     
     <Button size="small" className={classes.button} variant="contained" color="primary" className={classes.button} onClick={this.onAddChild} >
       Add Entry
     </Button>
    
     </Paper>
      
    );
  }
}

TimeSheet.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimeSheet);